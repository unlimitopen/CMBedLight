# CMBedLight
A Project for a bed light with pir sensors. We build in the year 2013 a light for a bedroom, because, if you need to wake up in the night and you won't bring all the lights up to wake up your wife, you can follow me by thoughts ? ;-), you need this really interessting build of bed light. In the time of 2013 no, really no bed manufactor has this light in there selling list.
And last but not least, it is really cheap, no really expensive components are implemented.

----

#### How does it works
 This development the firmware will produce a indirect light on a bed.
 If any person will come into the room or will get out of bed
 the light will be go on, after 20 Sec, or more,
 the lights will be go off by dim the leds. With a Encoder button
 the persons will be set time to bring up the light.
 the lights will be only lighting if value from fotoresistor will be
 set true. And what we have also implemented was a light for the headboard on bed.
 That you can use if you want to have a really nice night table lamp.

 The technical Enviroment is,
  * -> connect a rgb light stripe,
  * -> connect  a fotoresistor ,
  * -> connect n-mosfet transistors,
  * -> connect a encoder
  * -> setup interrupts on microcontroller
  * -> and give a little more place to creative work and development.

This was Build with a Atmega8 from Atmel Corp.

----

#### Version and History
| date | developer | version | answer / comment | actual version online |
| -------- | -------- | -------- | -------- | -------- |
| 2013/07/18 | c.weiligmann | 0.0.1 | initial release | |
| 2013/07/19 | c.weiligmann | 0.0.2 |  put a serial connection to debug this script. | |
| 2013/07/22 | c.weiligmann | 0.0.3 |  insert a Encoder Button for Setting up the time for lighting or date an time. | |
| 2013/07/23 | c.weiligmann | 0.0.4 | Powerup the N-Mofsets to make the lights on. | |
| 2013/07/23 | c.weiligmann | 0.0.5 | insert the a/d convert to read fototransistor. | |
| 2013/08/04 | c.weiligmann | 0.0.6 | insert a brightness value for fotoresistor to enable the leds | |
| 2013/08/05 | c.weiligmann | 0.0.7  | Insert a dim for leds in timer2 this is a way to switch of the leds very smoothly | |
| 2015/09/20 | c.möllers	| 0.0.8 |	Correction for flahing light after pwm dim was already ended | |
| 2015/09/20 | c.möllers | 0.0.9 | correction for the encoder button because, the button doesn't  work correctly | |
| 2015/09/21 | c.möllers | 0.0.9 | Save the configuration of rgb lights and timings into the local eeprom on microcontroller | |
| 2015/09/20 | c.möllers | 0.0.10 | Insert a time setup for dim the bed lights | |
| 2015/09/22 | c.möllers | 0.0.10 |	Insert a Secure Led flashing menu, to in which menu you are | <<<<<<<< |
| 2016/01/02 | c.möllers | 0.0.11 |	Insert a bluetooth module to connect that with a handy app, to define the rgb lights | development |
| 2016/05/03 | c.möllers | 0.0.12 |	Insert a rfm12 radio modul to communicate with the CM_CentralSensor System | development |


----

#### Actual Version
| Version | Build | developer |
| -------- | -------- | -------- |
| 0.0.10 | 20150922 | c.möllers |
