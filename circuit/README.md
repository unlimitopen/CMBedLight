# CMBedLight Circuit
![eagle view fo circuit](../pic/CMBedLight_eagle_schematic.png)  

![eagle view for bottom copper](../pic/CMBedLight_eagle_bottom_copper.png)  

![eagle view fo circuit](../pic/CMBedLight_motion-detection_eagle_schematic.png)  

![eagle view for bottom copper](../pic/CMBedLight_motion-detection_eagle_bottom_copper.png)  
